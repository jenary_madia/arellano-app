<html lang="en" dir="ltr" version="HTML+RDFa 1.1" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" class="js">

<head profile="http://www.w3.org/1999/xhtml/vocab">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="Generator" content="Drupal 7 (http://drupal.org)">
    <link rel="shortcut icon" href="https://www.arellano.edu.ph/sites/default/files/favicon-aulogo.png" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="sitelock-site-verification" content="7446">
    <title>Arellano University</title>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"> -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css') }}" media="all">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/css_vZ_wrMQ9Og-YPPxa1q4us3N7DsZMJa-14jShHgRoRNo.css') }}" media="all">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/css_iB3082BsVxuCeHgWZ2Z7j8uJbHWP8XjYWSIWmzlmOhU.css') }}" media="all">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/css_B9GexTMl7XnijFuwUyk8UAJ73t1UjAlQ9U2UFdFgpnM.css') }}" media="all">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/css_LzL9bvY-pDizynwzGrUHY2AX3bBG9WkiPWlGNnmNX4Q.css') }}" media="all">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alef:700&amp;subset=latin" media="all">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/css_059Iu9Hu9OFCrTkHRLSD9gpSyiUeMBzWuitT8p7sSwI.css') }}" media="all">

    <!--[if (lt IE 9)&(!IEMobile)]>
<link type="text/css" rel="stylesheet" href="css_TQHXanWBcfYt-VeSLLAi0LXtlVPHMnUTb6c-Wjv-cHQ.css" media="all" />
<![endif]-->

    <!--[if gte IE 9]><!-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/css_5CfSyN2Go2Ixemn2tBaqZtkWBcMERx5njCCjDi14yb8.css') }}" media="all">
    <!--<![endif]-->
    <script type="text/javascript" src="{{ asset('js/respond.min.js') }}"></script>
    <script type="text/javascript">
        <!--//--><![CDATA[//><!--
        document.cookie = 'adaptive_image=' + Math.max(screen.width, screen.height) + '; path=/';
        //--><!]]>
    </script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <script type="text/javascript" src="{{ asset('js/jssor.slider.min.js') }}"></script>
    <script>
        jssor_slider1_init = function () {
            var options = {
                $AutoPlay: 1,
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)
                $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$('slider1_container', options);
        };
    </script>
</head>

<body oncontextmenu="return false;" class="html front not-logged-in page-home responsive-menus-load-processed omega-mediaqueries-processed eu-cookie-compliance-processed responsive-layout-wide">
    <div id="omega-media-query-dummy">
        <style media="all">
            #omega-media-query-dummy {
                position: relative;
                z-index: -1;
            }
        </style>
        <!--[if (lt IE 9)&(!IEMobile)]><style media="all">#omega-media-query-dummy { z-index: 1; }</style><![endif]-->
        <style media="all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)">
            #omega-media-query-dummy {
                z-index: 0;
            }
        </style>
        <style media="all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)">
            #omega-media-query-dummy {
                z-index: 1;
            }
        </style>
        <style media="all and (min-width: 1220px)">
            #omega-media-query-dummy {
                z-index: 2;
            }
        </style>
    </div>
    <div id="skip-link">
        <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
    </div>
    <div class="page clearfix" id="page">
        <header id="section-header" class="section section-header">
            <div id="zone-menu-wrapper" class="zone-wrapper zone-menu-wrapper clearfix">
                <div id="zone-menu" class="zone zone-menu clearfix container-12">
                    <div class="grid-12 region region-menu" id="region-menu">
                        <div class="region-inner region-menu-inner">
                            <section class="block block-superfish block-1 block-superfish-1 odd" id="block-superfish-1">
                                <div class="block-inner clearfix">
                                    <h2 class="block-title">Main menu</h2>

                                    <div class="content clearfix">
                                        <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-pomegranate sf-total-items-7 sf-parent-items-3 sf-single-items-4 superfish-processed sf-js-enabled sf-shadow">
                                            <li id="menu-583-1" class="first odd sf-item-1 sf-depth-1 sf-total-children-6 sf-parent-children-0 sf-single-children-6 menuparent"><a href="/administration/about-ceo" title="about the ceo" class="sf-depth-1 menuparent sf-with-ul">Campus<span class="sf-sub-indicator"> »</span></a>
                                                <ul class="" style="float: none; width: 26.6667em;">
                                                    <li id="menu-678-1" class="first odd sf-item-1 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/administration/about-ceo" title="" class="sf-depth-2" style="float: none; width: auto;">About the CEO</a></li>
                                                    <li id="menu-587-1" class="middle even sf-item-2 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/administration/board-of-trustees" class="sf-depth-2" style="float: none; width: auto;">Board of Trustees</a></li>
                                                    <li id="menu-829-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/administration/corporate-officers" class="sf-depth-2" style="float: none; width: auto;">Corporate Officers</a></li>
                                                    <li id="menu-591-1" class="middle even sf-item-4 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/administration/officers-of-non-acad" class="sf-depth-2" style="float: none; width: auto;">Officers of Non-Academic Department</a></li>
                                                    <li id="menu-592-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/administration/general-administration" class="sf-depth-2" style="float: none; width: auto;">General Administration</a></li>
                                                    <li id="menu-14042-1" class="last even sf-item-6 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/administration/deans-and-principals" class="sf-depth-2" style="float: none; width: auto;">Deans of Colleges / Principals of Basic Education</a></li>
                                                </ul>
                                            </li>
                                            <li id="menu-831-1" class="middle even sf-item-2 sf-depth-1 sf-no-children"><a href="/students/academic-policies/admission-requirements" class="sf-depth-1">Admissions</a></li>
                                            <li id="menu-14051-1" class="middle odd sf-item-3 sf-depth-1 sf-no-children"><a href="/K-12%20Enrollment" title="" class="sf-depth-1">K-12 Pre Registration</a></li>
                                            <li id="menu-595-1" class="middle even sf-item-4 sf-depth-1 sf-total-children-3 sf-parent-children-2 sf-single-children-1 menuparent"><a href="/students/academic-policies/list-of-courses-offered" class="sf-depth-1 menuparent sf-with-ul">Programs<span class="sf-sub-indicator"> »</span></a>
                                                <ul class="sf-hidden" style="float: none; width: 18em;">
                                                    <li id="menu-1080-1" class="first odd sf-item-1 sf-depth-2 sf-total-children-2 sf-parent-children-0 sf-single-children-2 menuparent" style="white-space: normal; float: left; width: 100%;"><a href="/colleges/florentino-cayco-memorial-school-graduate-studies-school-education" title="" class="sf-depth-2 menuparent sf-with-ul" style="float: none; width: auto;">Academic Programs<span class="sf-sub-indicator"> »</span></a>
                                                        <ul class="sf-hidden" style="left: 18em; float: none; width: 12em;">
                                                            <li id="menu-675-1" class="first odd sf-item-1 sf-depth-3 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/colleges/florentino-cayco-memorial-school-graduate-studies-school-education" class="sf-depth-3" style="float: none; width: auto;">Colleges</a></li>
                                                            <li id="menu-1079-1" class="last even sf-item-2 sf-depth-3 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/students/academic-policies/list-of-courses-offered" title="" class="sf-depth-3" style="float: none; width: auto;">Programs Offered</a></li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-1090-1" class="middle even sf-item-2 sf-depth-2 sf-total-children-1 sf-parent-children-0 sf-single-children-1 menuparent" style="white-space: normal; float: left; width: 100%;"><a href="/comdev/arellano-university-community-development-program" title="" class="sf-depth-2 menuparent sf-with-ul" style="float: none; width: auto;">Non - Academic Program<span class="sf-sub-indicator"> »</span></a>
                                                        <ul class="sf-hidden" style="left: 18em; float: none; width: 15.6em;">
                                                            <li id="menu-1087-1" class="firstandlast odd sf-item-1 sf-depth-3 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/comdev/arellano-university-community-development-program" class="sf-depth-3" style="float: none; width: auto;">Community Development</a></li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-1641-1" class="last odd sf-item-3 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="http://www.arellano.edu.ph/international/inp" title="" class="sf-depth-2" style="float: none; width: auto;">International Nursing Program</a></li>
                                                </ul>
                                            </li>
                                            <li id="menu-1084-1" class="middle odd sf-item-5 sf-depth-1 sf-no-children"><a href="/research/research-and-publication-department-rpd" class="sf-depth-1">Research</a></li>
                                            <li id="menu-736-1" class="middle even sf-item-6 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent"><a href="/alumni/alumni-news" title="" class="sf-depth-1 menuparent sf-with-ul">Alumni<span class="sf-sub-indicator"> »</span></a>
                                                <ul class="sf-hidden" style="float: none; width: 27em;">
                                                    <li id="menu-730-1" class="first odd sf-item-1 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/alumni/about-our-alumni" class="sf-depth-2" style="float: none; width: auto;">About our Alumni</a></li>
                                                    <li id="menu-733-1" class="middle even sf-item-2 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/alumni/guiding-statements" class="sf-depth-2" style="float: none; width: auto;">Guiding Statements</a></li>
                                                    <li id="menu-734-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/alumni/vision-mission" class="sf-depth-2" style="float: none; width: auto;">Vision and Mission</a></li>
                                                    <li id="menu-14055-1" class="middle even sf-item-4 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/alumni/alumni-organizations" class="sf-depth-2" style="float: none; width: auto;">Alumni Organizations</a></li>
                                                    <li id="menu-14857-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/alumni/in-memoriam" class="sf-depth-2" style="float: none; width: auto;">In Memoriam</a></li>
                                                    <li id="menu-14860-1" class="middle even sf-item-6 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/alumni/alumni-services-department-community" class="sf-depth-2" style="float: none; width: auto;">Alumni Services Department Community &amp; Extension Programs</a></li>
                                                    <li id="menu-14862-1" class="last odd sf-item-7 sf-depth-2 sf-no-children" style="white-space: normal; float: left; width: 100%;"><a href="/alumni/objectives" class="sf-depth-2" style="float: none; width: auto;">Objectives</a></li>
                                                </ul>
                                            </li>
                                            <li id="menu-1640-1" class="last odd sf-item-7 sf-depth-1 sf-no-children"><a href="{{ route('login') }}" class="sf-depth-1">Login</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                            <section class="block block-views block-notifications-block-1 block-views-notifications-block-1 even" id="block-views-notifications-block-1">
                                <div class="block-inner clearfix">
                                    <h2 class="block-title" style="display: none;">Announcements</h2>

                                    <div class="content clearfix">
                                        <div class="view view-notifications view-id-notifications view-display-id-block_1 view-dom-id-1548d7bdda64bed24be186bbf51bc55c">

                                            <div class="view-content">
                                                <div class="views-row views-row-1 views-row-odd views-row-first">

                                                    <div class="views-field views-field-title"> <span class="field-content"><a href="/announcements/2019-annual-job-fair">2019 Annual Job Fair</a></span> </div>
                                                    <div class="views-field views-field-created"> <span class="views-label views-label-created">Post date: </span> <span class="field-content">Friday, Jan 4, 2019</span> </div>
                                                    <div class="views-field views-field-term-node-tid"> <span class="views-label views-label-term-node-tid">Branch Announcement: </span> <span class="field-content"></span> </div>
                                                </div>
                                                <div class="views-row views-row-2 views-row-even">

                                                    <div class="views-field views-field-title"> <span class="field-content"><a href="/announcements/urgent-hiring-0">Urgent Hiring!!</a></span> </div>
                                                    <div class="views-field views-field-created"> <span class="views-label views-label-created">Post date: </span> <span class="field-content">Monday, Sep 17, 2018</span> </div>
                                                    <div class="views-field views-field-term-node-tid"> <span class="views-label views-label-term-node-tid">Branch Announcement: </span> <span class="field-content"><a href="/branch-announcements/main">Main</a></span> </div>
                                                </div>
                                                <div class="views-row views-row-3 views-row-odd views-row-last">

                                                    <div class="views-field views-field-title"> <span class="field-content"><a href="/announcements/general-orientation-college-freshmen-and-transferees">General Orientation for College Freshmen and Transferees</a></span> </div>
                                                    <div class="views-field views-field-created"> <span class="views-label views-label-created">Post date: </span> <span class="field-content">Tuesday, Jul 3, 2018</span> </div>
                                                    <div class="views-field views-field-term-node-tid"> <span class="views-label views-label-term-node-tid">Branch Announcement: </span> <span class="field-content"><a href="/branch-announcements/main">Main</a></span> </div>
                                                </div>
                                            </div>

                                            <div class="more-link">
                                                <a href="/Announcements">
    See more...  </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="block block-views block-hot-links-block block-views-hot-links-block odd" id="block-views-hot-links-block">
                                <div class="block-inner clearfix">
                                    <h2 class="block-title" style="display: none;">Hotlinks</h2>

                                    <div class="content clearfix">
                                        <div class="view view-hot-links view-id-hot_links view-display-id-block view-dom-id-cd204d217c7a9b1e30f70eb01e7d2ff2">

                                            <div class="view-content">
                                                <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">

                                                    <div class="views-field views-field-title"> <span class="views-label views-label-title">Hotlinks</span> <span class="field-content"><a href="/node/26">Hot Links</a></span> </div>
                                                    <div class="views-field views-field-body">
                                                        <div class="field-content">
                                                            <p>
                                                                <a href="http://www.arellanolaw.edu/" title="www.arellanolaw.edu"><img alt="www.arellanolaw.edu" src="https://arellano.edu.ph/sites/default/files/au_law_0.png" style="height:33px; width:33px"></a>&nbsp;&nbsp;
                                                                <a href="http://www.lawphil.net/" title="www.lawphil.net"><img alt="" src="https://arellano.edu.ph/sites/default/files/Law-Phil-Final-logo-c.png" style="height:33px; width:33px"></a>&nbsp;&nbsp;
                                                                <a href="https://www.facebook.com/ArellanoUniversityOfficial" title="www.facebook.com/ArellanoUniversityOfficial"><img alt="" src="https://arellano.edu.ph/sites/default/files/facebook.png" style="height:33px; width:33px"></a>&nbsp;&nbsp;
                                                                <a href="https://twitter.com/Arellano_U" title="twitter.com/Arellano_U"><img alt="" src="https://arellano.edu.ph/sites/default/files/Twitter_bird_icon.png" style="height:33px; width:33px"></a>&nbsp;&nbsp;
                                                                <a href="https://arellano.edu.ph/international" title="arellano.edu.ph/international"><img alt="" src="https://arellano.edu.ph/sites/default/files/ipd_1.png" style="height:33px; width:33px"></a>&nbsp;&nbsp;
                                                                <a href="http://mail.arellano.edu.ph/owa/auth/logon.aspx?url=http://mail.arellano.edu.ph/owa/&amp;reason=0" title="mail.arellano.edu.ph"><img alt="" src="https://arellano.edu.ph/sites/default/files/email-btn.png" style="height:33px; width:33px"></a>&nbsp;&nbsp;
                                                                <br>
                                                                <a href="https://www.arellano.edu.ph/it-department/otas/" title="it-department/otas"><img alt="IT Online Technical Assistance System" src="https://www.arellano.edu.ph/sites/default/files/AU%20Tech%20logs%20Logo%20Web.png" style="height:36px; width:36px"></a>&nbsp;&nbsp;
                                                                <a href="https://schools.jobs180.com/AU" title="schools.jobs180.com/AU"><img alt="AU ResumeLink" src="https://www.arellano.edu.ph/sites/default/files/jobs180.png" style="height:36px; width:36px"></a>&nbsp;&nbsp;
                                                                <a href="https://arellanoinp.com" title="Computer Based Evaluation INP"><img alt="INP CBE" src="https://www.arellano.edu.ph/sites/default/files/cbe-moodle-cap_one.png" style="height:36px; width:36px"></a>&nbsp;&nbsp;
                                                                <a href="https://support.office.com/en-us/article/Office-365-basics-video-training-396b8d9e-e118-42d0-8a0d-87d1f2f055fb" title="Office 365 Tutorial"><img alt="MS Office 365" src="https://www.arellano.edu.ph/sites/default/files/office365-3.png" style="height:36px; width:36px"></a>&nbsp;&nbsp;
                                                                <a href="https://arellano.edu.ph/the-standard/june-september-2018" title="The Standard News"><img alt="The Standard" src="https://www.arellano.edu.ph/sites/default/files/the_standard_one.png" style="height:36px; width:36px"></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="block block-views block-home-btn-block block-views-home-btn-block even block-without-title" id="block-views-home-btn-block">
                                <div class="block-inner clearfix">

                                    <div class="content clearfix">
                                        <div class="view view-home-btn view-id-home_btn view-display-id-block view-dom-id-56ff67231682f3148102f9ff899a4727">

                                            <div class="view-content">
                                                <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">

                                                    <div class="views-field views-field-body">
                                                        <div class="field-content">
                                                            <p>
                                                                <a href="https://arellano.edu.ph/"><img alt="" src="https://arellano.edu.ph/sites/default/files/home-btn.png" style="height:30px; width:30px"></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section class="block block-search block-form block-search-form odd" id="block-search-form">
                                <div class="block-inner clearfix">
                                    <h2 class="block-title">Enter your keyword</h2>

                                    <div class="content clearfix">
                                        <form action="/" method="post" id="search-block-form" accept-charset="UTF-8">
                                            <div>
                                                <div class="container-inline">
                                                    <div class="form-item form-type-textfield form-item-search-block-form">
                                                        <label class="element-invisible" for="edit-search-block-form--2">Search </label>
                                                        <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text">
                                                    </div>
                                                    <div class="form-actions form-wrapper" id="edit-actions">
                                                        <input type="submit" id="edit-submit" name="op" value="Search" class="form-submit">
                                                    </div>
                                                    <input type="hidden" name="form_build_id" value="form-d54lsG2vlN1wx-FJjcpisnKHTuw6a26e60ysbYwSy_I">
                                                    <input type="hidden" name="form_id" value="search_block_form">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">
                <div id="zone-branding" class="zone zone-branding clearfix container-12">
                    <div class="grid-8 prefix-2 region region-branding" id="region-branding">
                        <div class="region-inner region-branding-inner">
                            <div class="branding-data clearfix">
                                <div class="logo-img">
                                    <a href="/" rel="home" title="" class="active"><img src="https://www.arellano.edu.ph/sites/default/files/logo1_3.png" alt="" id="logo"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section id="section-content" class="section section-content">
            <div id="slider1_container" style="margin: 0 auto;position: relative; width: 1200px;
                height: 300px; ">

                <!-- Slides Container -->
                <div data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 1200px; height: 300px;
                    overflow: hidden;">
                    @foreach($posts as $post)
                        @if($post->type == 4)
                            <div><img data-u="image" src="{{ asset('storage/posts/'.$post->image) }}" /></div>
                        @endif
                    @endforeach
                </div>
                
                <!--#region Arrow Navigator Skin Begin -->
                <!-- Help: https://www.jssor.com/development/slider-with-arrow-navigator.html -->
                <style>
                    .jssora051 {display:block;position:absolute;cursor:pointer;}
                    .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
                    .jssora051:hover {opacity:.8;}
                    .jssora051.jssora051dn {opacity:.5;}
                    .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
                </style>
                <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                    </svg>
                </div>
                <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                    </svg>
                </div>
                <!--#endregion Arrow Navigator Skin End -->

                <!-- Trigger -->
                <script>
                    jssor_slider1_init();
                </script>
            </div>
            <br>    
            <div id="zone-preface-wrapper" class="zone-wrapper zone-preface-wrapper clearfix">
                <div id="zone-preface" class="zone zone-preface clearfix container-12">
                    <div class="grid-4 region region-preface-first" id="region-preface-first">
                        <div class="region-inner region-preface-first-inner">
                            <section class="block block-views block-sport-events-block block-views-sport-events-block odd" id="block-views-sport-events-block">
                                <div class="block-inner clearfix">
                                    <h2 class="block-title">Sports Updates</h2>

                                    <div class="content clearfix">
                                        <div class="view view-sport-events view-id-sport_events view-display-id-block view-dom-id-0bab3239770c9bff292a389c8050e865">

                                            <div class="view-content">
                                              @foreach($posts as $post)
                                                @if($post->type == 3)
                                                  <div class="views-row views-row-1 views-row-odd views-row-first">
                                                    <div class="views-field views-field-title"> <span class="field-content"><a href="{{ url("/announcements/$post->id") }}">{{ $post->caption }}</a></span> </div>
                                                    <div class="views-field views-field-created"> <span class="views-label views-label-created">Post date: </span> <span class="field-content">{{ $post->created_at }}</span> </div>
                                                </div>
                                                @endif
                                              @endforeach
                                                
                                                <!-- <div class="views-row views-row-2 views-row-even">

                                                    <div class="views-field views-field-title"> <span class="field-content"><a href="/sports-events/cholo-martin-new-au-senior-basketball-varsity-team%E2%80%99s-coach">CHOLO MARTIN IS THE NEW AU SENIOR BASKETBALL VARSITY TEAM’S  COACH </a></span> </div>
                                                    <div class="views-field views-field-created"> <span class="views-label views-label-created">Post date: </span> <span class="field-content">Thursday, Oct 25, 2018</span> </div>
                                                </div>
                                                <div class="views-row views-row-3 views-row-odd views-row-last">

                                                    <div class="views-field views-field-title"> <span class="field-content"><a href="/sports-events/jiovani-jalalon-comes-home-headline-ncaa-all-star-side-events">Jiovani Jalalon comes home to headline NCAA All-Star Side Events</a></span> </div>
                                                    <div class="views-field views-field-created"> <span class="views-label views-label-created">Post date: </span> <span class="field-content">Wednesday, Aug 29, 2018</span> </div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="grid-4 region region-preface-second" id="region-preface-second">
                        <div class="region-inner region-preface-second-inner">
                            <section class="block block-views block-news-and-articles-block block-views-news-and-articles-block odd" id="block-views-news-and-articles-block">
                                <div class="block-inner clearfix">
                                    <h2 class="block-title">News and Events</h2>

                                    <div class="content clearfix">
                                        <div class="view view-news-and-articles view-id-news_and_articles view-display-id-block view-dom-id-f21a0d31b85a7917e900f007737579fa">

                                            <div class="view-content">
                                                @foreach($posts as $post)
                                                    @if($post->type == 2)
                                                      <div class="views-row views-row-1 views-row-odd views-row-first">
                                                        <div class="views-field views-field-title"> <span class="field-content"><a href="{{ url("/announcements/$post->id") }}">{{ $post->caption }}</a></span> </div>
                                                        <div class="views-field views-field-created"> <span class="views-label views-label-created">Post date: </span> <span class="field-content">{{ $post->created_at }}</span> </div>
                                                    </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="grid-4 region region-preface-third" id="region-preface-third">
                        <div class="region-inner region-preface-third-inner">
                            <section class="block block-block block-10 block-block-10 odd" id="block-block-10">
                                <div class="block-inner clearfix">
                                    <h2 class="block-title">Announcements</h2>

                                    <div class="content clearfix">
                                        <div class="view view-news-and-articles view-id-news_and_articles view-display-id-block view-dom-id-f21a0d31b85a7917e900f007737579fa">

                                            <div class="view-content">
                                                @foreach($posts as $post)
                                                    @if($post->type == 1)
                                                      <div class="views-row views-row-1 views-row-odd views-row-first">
                                                        <div class="views-field views-field-title"> <span class="field-content"><a href="{{ url("/announcements/$post->id") }}">{{ $post->caption }}</a></span> </div>
                                                        <div class="views-field views-field-created"> <span class="views-label views-label-created">Post date: </span> <span class="field-content">{{ $post->created_at }}</span> </div>
                                                    </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        <!-- <p><a href="andres-bonifacio-campus/welcome-andres-bonifacio-campus-arellano-university-pasig-city">Andres Bonifacio Campus, Arellano University in&nbsp;Pasig City</a></p>
                                        <p><a href="plaridel-campus/welcome-plaridel-campus-arellano-university-mandaluyong-city">Plaridel Campus, Arellano University in&nbsp;Mandaluyong City</a></p>
                                        <p><a href="jose-abad-santos-campus/welcome-jose-abad-santos-campus-arellano-university-pasay">Jose Abad Santos / Apolinario Mabini Campus, Arellano University in Pasay CIty</a></p>
                                        <p><a href="elisa-esguerra-campus/welcome-elisa-esguerra-campus-arellano-university-malabon-city">Elisa Esguerra Campus /&nbsp;Jose Rizal Campus, Arellano Unviersity in Malabon City</a></p>
                                        <div><a href="http://www.arellanolaw.edu/">School of Law Apolinario Mabini Campus, Arellano University in Menlo, Pasay City</a></div>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p> -->
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">
                <div id="zone-content" class="zone zone-content clearfix container-12">

                    <aside class="grid-3 region region-sidebar-first" id="region-sidebar-first">
                        <div class="region-inner region-sidebar-first-inner">
                        </div>
                    </aside>
                    <div class="grid-9 region region-content" id="region-content">
                        <div class="region-inner region-content-inner">
                            <a id="main-content" style="font-size: 0px;"></a>
                            <div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
                                <div class="block-inner clearfix">

                                    <div class="content clearfix">
                                        <div class="panel-display omega-grid omega-12-threecol-4-4-4">
                                            <div class="panel-panel grid-4">
                                                <div class="inside"></div>
                                            </div>
                                            <div class="panel-panel grid-4">
                                                <div class="inside"></div>
                                            </div>
                                            <div class="panel-panel grid-4">
                                                <div class="inside"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="section-footer" class="section section-footer">
            <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">
                <div id="zone-footer" class="zone zone-footer clearfix container-12">
                    <div class="grid-3 region region-footer-first" id="region-footer-first">
                        <div class="region-inner region-footer-first-inner">
                            <div class="block block-block block-5 block-block-5 odd block-without-title" id="block-block-5">
                                <div class="block-inner clearfix">

                                    <div class="content clearfix">
                                        <div>
                                            <p>Arellano University, Manila
                                                <br> 2600 Legarda St. Sampaloc Manila
                                                <br> Tel. No. 734-7371 to 79 Local 216 (Trunkline)
                                                <br> Copyrights @ 2013 | arellano.edu.ph</p>
                                            <p><span id="siteseal"><img style="cursor:pointer;cursor:hand" src="https://seal.godaddy.com/images/3/en/siteseal_gd_3_h_d_m.gif" onclick="verifySeal();" alt="SSL site seal - click to verify"></span></p>
                                            <script async="" type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=aZeHtrESMY9fBmLicxwnU9UTIQJuHg8h5IZEIGNpa0yR7TA0ltca7"></script>
                                            <p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-7 prefix-2 region region-footer-second" id="region-footer-second">
                        <div class="region-inner region-footer-second-inner">
                            <section class="block block-superfish block-2 block-superfish-2 odd" id="block-superfish-2">
                                <div class="block-inner clearfix">
                                    <h2 class="block-title">Footer Menu</h2>

                                    <div class="content clearfix">
                                        <ul id="superfish-2" class="menu sf-menu sf-menu-footer-menu sf-navbar sf-style-blue sf-total-items-6 sf-parent-items-0 sf-single-items-6 superfish-processed sf-js-enabled sf-shadow">
                                            <li id="menu-972-2" class="first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/about-us/arellano-university-logo" class="sf-depth-1">About Us</a></li>
                                            <li id="menu-967-2" class="middle even sf-item-2 sf-depth-1 sf-no-children"><a href="/administration/about-ceo" title="" class="sf-depth-1">Administrator</a></li>
                                            <li id="menu-968-2" class="middle odd sf-item-3 sf-depth-1 sf-no-children"><a href="/students/academic-policies/list-of-courses-offered" title="" class="sf-depth-1">Programs Offered</a></li>
                                            <li id="menu-969-2" class="middle even sf-item-4 sf-depth-1 sf-no-children"><a href="/alumni/about-our-alumni" title="" class="sf-depth-1">Alumni</a></li>
                                            <li id="menu-970-2" class="middle odd sf-item-5 sf-depth-1 sf-no-children"><a href="/sports/sport-events" title="" class="sf-depth-1">Sports</a></li>
                                            <li id="menu-971-2" class="last even sf-item-6 sf-depth-1 sf-no-children"><a href="/content/contacts" title="" class="sf-depth-1">Contacts</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="region region-page-bottom" id="region-page-bottom">
        <div class="region-inner region-page-bottom-inner">
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>




</body>

</html>