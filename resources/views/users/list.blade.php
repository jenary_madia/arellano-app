@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url('user') }}" class="btn btn-primary">Add User</a>
            <br>
            <br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Username</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Role</th>
                        <th scope="col">Department</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            @switch($user->role)
                                @case(1)
                                        <td>Admin</td>
                                    @break

                                @case(2)
                                        <td>Dean</td>
                                    @break
                                @case(3)
                                        <td>Marketing</td>
                                    @break
                            @endswitch
                            <td>{{ isset($user->department) ? $user->department->name : "" }}</td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ url('/user/'.$user->id) }}">EDIT</a>
                                <button value="{{ $user->id }}" class="btn btn-danger btn-sm btn-delete">DELETE</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $( document ).ready(function() {
        $(".btn-delete").on("click",function(e) {
            e.preventDefault();
            axios.post(base_url+'/users/change-status',{
                user_id : $(this).val(),
                status : 0
            })
            .then(function (response) {
                alert(response.data.message);
                location.reload();
            })
            .catch(function (error) {
                console.log(error);
            });
        })
    });
</script>
@endsection
