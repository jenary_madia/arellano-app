@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <a href="{{ url('/users') }}"><< Back to user list</a>
            <br>
            <br>
            <div class="card">
                <h5 class="card-header">{{ $title }}</h5>
                <div class="card-body">
                    <form id="details_form">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" value="{{ $user ? $user->username : '' }}" class="form-control" name="username" placeholder="Enter username">
                        </div>
                        <div class="form-group">
                            <label for="fullname">Full Name</label>
                            <input type="text" value="{{ $user ? $user->name : '' }}" class="form-control" name="fullname" placeholder="Enter full name">
                        </div>
                         <div class="form-group">
                            <label for="fullname">Email</label>
                            <input type="email" value="{{ $user ? $user->email : '' }}" class="form-control" name="email" placeholder="Enter Email">
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select name="role" id="role" class="form-control">
                                <option value="0">-- Select Role --</option>
                                <option value="1">Admin</option>
                                <option value="2">Dean</option>
                                <option value="3">Marketing</option>
                            </select>
                        </div>
                        <div class="form-group div-department">
                            <label for="department">Department</label>
                            <select name="department" id="department" class="form-control">
                                @foreach($departments as $id => $name)
                                    <option {{ $user ? ($user->dept_id == $id ? "selected" : "") : "" }} value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" value="{{ $user ? $user->id : '' }}" id="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $( document ).ready(function() {
        $("#role").on("change",function(e) {
            if($(this).val() != 2)
                $('.div-department').hide();
            else
                $('.div-department').show();
        })
        $("#submit").on("click",function(e) {
            e.preventDefault()
            axios.post($(this).val() ? base_url+'/user/'+$(this).val() : base_url+'/user',$("#details_form").serialize())
            .then(function (response) {
                if(response.data.success) {
                    alert(response.data.message);
                    location.replace(base_url+'/users')
                }else{
                    var text = "";
                    $.each(response.data.errors , function(i , e)
                    {
                        text = text + e + "\n";
                    });
                    alert(text);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        })
    });
</script>
@endsection
