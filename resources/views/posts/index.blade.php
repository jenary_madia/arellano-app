@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url('details-post') }}" class="btn btn-primary">Add Post</a>
            <br>
            <br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Post By</th>
                        <th scope="col">Department</th>
                        <th scope="col">Type</th>
                        <th scope="col">Caption</th>
                        <th scope="col">Date posted</th>
                        <th>Action</th>
                    </tr>
                </thead>
                    @foreach($posts as $post)
                        @if($post->is_deleted == 0)
                        <tr>
                            <td>{{ $post->name }}</td>
                            <td>{{ $post->dept_name }}</td>
                                @switch($post->type)
                                    @case(1)
                                        <td>Announcements</td>
                                        @break

                                    @case(2)
                                        <td>News/Events</td>
                                        @break

                                    @case(3)
                                        <td>Sports</td>
                                        @break
                                    @case(4)
                                        <td>Banner</td>
                                        @break
                                    @default
                                        <td></td>
                                        @break
                                @endswitch
                            <td>{{ $post->caption }}</td>
                            <td>{{ $post->created_at }}</td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ url('/details-post/'.$post->id) }}">EDIT</a>
                                <button value="{{ $post->id }}" class="btn btn-danger btn-sm btn-delete">DELETE</button>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                <tbody>
                    
                </tbody>
            </table>
            <br>
            <br>
            <br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Post By</th>
                        <th scope="col">Department</th>
                        <th scope="col">Type</th>
                        <th scope="col">Caption</th>
                        <th scope="col">Date posted</th>
                        <th>Action</th>
                    </tr>
                </thead>
                    @foreach($posts as $post)
                        @if($post->is_deleted == 1)
                        <tr>
                            <td>{{ $post->name }}</td>
                            <td>{{ $post->dept_name }}</td>
                                @switch($post->type)
                                    @case(1)
                                        <td>Announcements</td>
                                        @break

                                    @case(2)
                                        <td>News/Events</td>
                                        @break

                                    @case(3)
                                        <td>Sports</td>
                                        @break
                                    @case(4)
                                        <td>Banner</td>
                                        @break
                                    @default
                                        <td></td>
                                        @break
                                @endswitch
                            <td>{{ $post->caption }}</td>
                            <td>{{ $post->created_at }}</td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ url('/details-post/'.$post->id) }}">EDIT</a>
                                <button value="{{ $post->id }}" class="btn btn-danger btn-sm btn-delete">DELETE</button>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $( document ).ready(function() {
        $(".btn-delete").on("click",function(e) {
            e.preventDefault();
            axios.post(base_url+'/posts/soft-delete',{
                post_id : $(this).val(),
            })
            .then(function (response) {
                alert(response.data.message);
                location.reload();   
            })
            .catch(function (error) {
                console.log(error);
            });
        })
    });
</script>
@endsection

