@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
        <div class="col-md-8 mx-auto box">
            <a href="{{ url('/posts') }}"><< Back to post list</a>
            <br>
            <br>
            <div class="card">
                <div class="card-header">
                    <span class="float-left">Name of user : <strong>{{ Auth::user()->name }}</strong></strong></span>
                    <span class="float-right">Department : <strong>{{ isset(Auth::user()->department) ? Auth::user()->department->name : ( Auth::user()->role == 1 ? "Admin" : "Marketing" ) }}</strong></span>
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <label for="caption">Subject</label>
                            <input type="text" value="{{ $post ? $post->caption : '' }}" class="form-control" id="caption" placeholder="Enter caption">
                        </div>
                        <div class="form-group">
                        @if($post)
                            <input hidden  type="text" value="{{ $post->type }}" class="form-control" id="type" placeholder="Enter caption">
                            @switch($post->type)
                                @case(1)
                                    <input disabled type="text" value="Announcements" class="form-control" placeholder="Enter caption">
                                @break
                                @case(2)
                                    <input disabled type="text" value="News/Events" class="form-control" placeholder="Enter caption">
                                @break
                                @case(3)
                                    <input disabled type="text" value="Sports" class="form-control" placeholder="Enter caption">
                                @break
                                @case(4)
                                    <input disabled type="text" value="Banner" class="form-control" placeholder="Enter caption">
                                @break
                            @endswitch
                        @else
                            <label for="caption">Type</label>
                            @if(Auth::user()->role == 2)
                            <input hidden  type="text" value="1" class="form-control" id="type" placeholder="Enter caption">
                            <input disabled type="text" value="Announcements" class="form-control" placeholder="Enter caption">
                            @else
                                <select name="" id="type" class="form-control">
                                    <option value="0"> -- Select Type -- </option>
                                    <option value="1">Announcements</option>
                                    @if(Auth::user()->role != 2)
                                        @if(Auth::user()->role != 3)
                                            <option value="2">News/Events</option>
                                        @endif
                                        <option value="3">Sports</option>
                                        <option value="4">Banner</option>
                                    @endif
                                </select>
                            @endif
                        @endif
                        </div>
                        @if($type)
                        <br>
                        @endif
                        @if(Auth::user()->role != 2)
                            <div class="form-group div-image">
                                @if(! isset($post))
                                    <label for="image">Image</label>
                                    <input type="file" id="image">
                                @else
                                    @if($type)
                                    <section id="sec-current">
                                        @if($type == "image")
                                            <img src='{{URL::asset("/storage/posts/$post->image")}}' alt="" width="100%">
                                        @else
                                            <video width="100%" controls>
                                                <source src='{{URL::asset("/storage/posts/$post->image")}}' type="video/mp4">
                                            </video>
                                        @endif
                                        <br>
                                        <br>
                                        <button class="btn btn-danger btn-sm btn-change">Change Image/Video</button>
                                    </section>
                                    @endif
                                @endif
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="image">Description</label>
                            <textarea placeholder="Enter description" style="height: 170px" id="description" class="form-control">{{ isset($post) ? $post->description : "" }}</textarea>
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <button type="submit" value="{{ $post ? $post->id : '' }}" id="submit" class="btn btn-primary">{{ isset($post) ? "Update" : "Add" }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection
@section('scripts')
<script>
    $( document ).ready(function() {
        $(".btn-change").on("click",function(e) {
            e.preventDefault()
            $('#sec-current').empty()
            $('#sec-current').append(`<label for="image">Image</label>
                            <input type="file" id="image">`)
        })

        $("#type").on("change",function(e) {
            if($(this).val() == 1)
                $('.div-image').hide();
            else
                $('.div-image').show();
        })

        
        $("#submit").on("click",function(e) {
            e.preventDefault()
            var formData = new FormData();
            var imagefile = document.querySelector('#image');
            formData.append("image", imagefile ? imagefile.files[0] : null);
            formData.append("type", $("#type").val());
            formData.append("caption", $("#caption").val());
            formData.append("description", $("#description").val());
            axios.post($(this).val() ? base_url+'/details-post/'+$(this).val() : base_url+'/details-post', formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
            }).then(function (response) {
                if(response.data.success) {
                    alert(response.data.message);
                    location.replace(base_url+'/posts')
                }else{
                    var text = "";
                    $.each(response.data.errors , function(i , e)
                    {
                        text = text + e + "\n";
                    });
                    alert(text);
                }
            })

        })
    });
</script>
@endsection
