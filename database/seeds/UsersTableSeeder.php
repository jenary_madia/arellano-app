<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	DB::table('users')->insert([
            "username" => "admin",
			"name" => "Super Admin",
			"role" => 1,
			"email" => "admin2314@gmail.com",
			"email_verified_at" => Carbon::now(),
			"password" => bcrypt('secret'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);   
    }
}
