<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\User;
use Carbon\Carbon;
use DB;
use Response;
use Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    public function index() {
        $users = User::with("department")->where([
            'active' => 1,
        ])
        ->where('id','!=',Auth::User()->id)
        ->get();
    	return view("users.list",compact('users'));
    }

    public function show($id = null) {
    	$id ? $title = "Update User" : $title = "Add User";
    	$departments = Department::pluck('name','id');
        $user = User::find($id);
    	return view("users.details",compact('title','departments','user'));
    }

    public function updateOrCreate(Request $request,$id = null) {
        $validator = Validator::make($request->all(), [
            'username' => "required|".($id ? "unique:users,id,".$id : "unique:users"),
            'fullname' => "required",
            'email' => "required",
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->all(),
            ]);
            
        }
        DB::beginTransaction();
        try {
            User::updateOrCreate(
                ["id" => $id],
                [
                    "username" => $request->username,
                    "name" => $request->fullname,
                    "role" => $request->role,
                    "dept_id" => $request->role == 2 ? $request->department : null,
                    "email" => $request->email,
                    "email_verified_at" => Carbon::now(),
                    "password" => bcrypt($request->username),
                ]
            ); 
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => $id ? "User details successfully updated" : "User successfully added",
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    	
    }

    public function changePassword() {
        return view("auth.passwords.change");
    }

    public function changePass(Request $request) {
        if(Auth::Check()){
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $validator->errors()->all(),
                ]);
            }else{  
                DB::beginTransaction();
                try {
                    $current_password = Auth::User()->password;           
                    if(Hash::check($request_data['current-password'], $current_password)){           
                        $user_id = Auth::User()->id;                       
                        $obj_user = User::find($user_id);
                        $obj_user->password = Hash::make($request_data['password']);
                        $obj_user->save(); 
                    }else{           
                        $error = ['Please enter correct current password'];
                        return response()->json([
                            'success' => false,
                            'errors' => $error
                        ]);   
                    }
                    DB::commit();
                    return response()->json([
                        'success' => true,
                        'message' => "Password successfully updated.",
                    ]);
                } catch (Exception $e) {
                    DB::rollback();
                }
            }        
        }else{
            return redirect()->to('/');
        } 
    }

    public function admin_credential_rules(array $data)
    {
        $messages = [
            'current-password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
        ];

        $validator = Validator::make($data, [
            'current-password' => 'required',
            'password' => 'required|same:password',
            'password_confirmation' => 'required|same:password',     
        ], $messages);

        return $validator;
    } 

    public function changeStatus(Request $request) {
        DB::beginTransaction();
        try {
            $user_id = $request->user_id;
            $status = $request->status;
            User::where([
                'id' => $user_id
            ])->update([
                "active" => $status
            ]);   
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "User successfully deleted.",
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }
}
