<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use File;
use DB;
use App\Post;
use Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class PostController extends Controller
{
    public function index() {
        $posts = Post::join('users','posts.posted_by','users.id')
        ->leftJoin('departments','users.dept_id','departments.id')
        ->selectRaw("posts.*,users.name,departments.name as dept_name")
        ->where("posted_by", Auth::user()->id)
        // ->where("is_deleted", 0)
        ->get();
    	return view('posts.index',compact('posts'));
    }

    public function show($id = null) {
        $id ? $title = "Update Post" : $title = "Add Post";
        $post = Post::join("users","users.id","posts.posted_by")
        ->leftJoin("departments","users.dept_id","departments.id")
        ->where([
            "posts.id" => $id,
            "posted_by" => Auth::user()->id,
        ])
        ->selectRaw("posts.*,departments.id as dept_id")
        ->first();
        $dept_id = Auth::user()->dept_id;
        if($post) {
            $dept_id = $post->dept_id;
            if($post->image) {
                if(in_array(explode('.',$post->image)[1], ['jpeg','jpg','png','PNG','JPG'])) {
                    $type = 'image';
                }else{
                    $type = 'video';
                }
            }else{
                $type = null;
            }
        }else{
            $type = null;
        }
    	return view('posts.details',compact('title','post','type','dept_id'));
    }

    public function updateOrCreate(Request $request,$id = null) {
        $rules = [
            'caption' => 'required',
        ];
        $dept_id = Auth::user()->dept_id;

        if($id && $request->image != "null") {
            $rules['description'] = 'required';
        }
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->all(),
            ]);
            
        }
        $path = 'public/posts/';
        if($id) {
            $post = Post::find($id);  
            if($request->type != 1) {
                if($request->image != "null") {
                    if($request->image != "undefined") { 
                        Storage::delete([
                            $path.$post->image
                        ]);             
                    }
                }
            }
            Storage::delete([
                $path.$post->id.'txt'
            ]);
            Storage::delete([
                $path.$post->id.'-desc.txt'
            ]);  
        }

        if($request->type != 1) {
            if($request->image != "null") {
                if($request->image != "undefined") {
                    Storage::putFile(
                        'public/posts', $request->file('image')
                    );
                }
            }
        }

        DB::beginTransaction();
        try {
            $params = [
                "posted_by" => Auth::user()->id,
                "type" => $request->type,
                "caption" => $request->caption,
                "description" => $request->description,
            ];

            if(Auth::user()->role != 2) {
                if($request->type != 1) {

                    if($id) {
                        if($request->image != "null") 
                            $params["image"] = $request->image->hashName();
                    }else{
                        $params["image"] = $request->image->hashName();
                    }
                }
            }


            $post = Post::updateOrCreate(
                ["id" => $id],
                $params
            ); 

            Storage::put("$path$post->id.txt", $request->caption);
            Storage::put("$path$post->id-desc.txt", $request->description);

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => $id ? "Post successfully updated" : "Post successfully added",
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }

    	
    }

    public function posts($post_id = null) {
        $post = Post::join('users','posts.posted_by','users.id')
        ->leftJoin('departments','users.dept_id','departments.id')
        ->selectRaw("posts.*,DATE_FORMAT(posts.created_at, '%a %M %d, %Y') as date_posted,users.name,departments.name as dept_name")
        ->where('posts.id',$post_id)->first();

        $type = null;

        if($post->image) {
            if(in_array(explode('.',$post->image)[1], ['jpeg','jpg','png','PNG','JPG'])) {
                $type = 'image';
            }else{
                $type = 'video';
            }
        }else{
            $type = null;
        }


        return view("posts.post",compact("post","type"));
    }

    public function delete(Request $request) {
        $path = 'public/posts/';
        DB::beginTransaction();
        try {
            $post_id = $request->post_id;
            $post = Post::where([
                'id' => $post_id
            ])->first();   

            Storage::delete([
                $path.$post->id.'.txt'
            ]); 

            Storage::delete([
                $path.$post->image
            ]); 

            Storage::delete([
                $path.$post->id.'-desc.txt'
            ]);  

            $post->delete();
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Post successfully deleted.",
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function softDelete(Request $request) {
        $path = 'public/posts/';
        $new_path = 'public/removed/';
        DB::beginTransaction();
        try {
            $post_id = $request->post_id;
            // return  $post_id;
            $post = Post::where([
                'id' => $post_id
            ])->first();   
            // if(! Storage::exists($new_path.$post->id.'.txt'))
            //     Storage::move($path.$post->id.'.txt',$new_path.$post->id.'.txt');
            // if(! Storage::exists($new_path.$post->id.'-desc.txt'))
            //     Storage::move($path.$post->id.'-desc.txt',$new_path.$post->id.'-desc.txt');
            // if(! Storage::exists($new_path.$post->image))
            //     Storage::move($path.$post->image,$new_path.$post->image);

            
            $post->update([
                'is_deleted' => 1
            ]);
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Post successfully deleted.",
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function retrieve(Request $request) {
        $path = 'public/removed/';
        $new_path = 'public/posts/';
        DB::beginTransaction();
        try {
            $post_id = $request->post_id;
            // return  $post_id;
            $post = Post::where([
                'id' => $post_id
            ])->first();   
            if(! Storage::exists($new_path.$post->id.'.txt'))
                Storage::move($path.$post->id.'.txt',$new_path.$post->id.'.txt');
            if(! Storage::exists($new_path.$post->id.'-desc.txt'))
                Storage::move($path.$post->id.'-desc.txt',$new_path.$post->id.'-desc.txt');
            if(! Storage::exists($new_path.$post->image))
                Storage::move($path.$post->image,$new_path.$post->image);

            
            $post->update([
                'is_deleted' => 0,
                'created_at' => Carbon::now()
            ]);
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Post successfully updated.",
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }
}
