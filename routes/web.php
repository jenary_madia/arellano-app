<?php

    use App\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $posts = Post::join('users','posts.posted_by','users.id')
        ->leftJoin('departments','users.dept_id','departments.id')
        ->selectRaw("posts.*,DATE_FORMAT(posts.created_at, '%a %M %d, %Y') as date_posted,users.name,departments.name as dept_name,users.dept_id")
        ->orderBy('id','desc')
        ->get();
    return view('welcome',compact('posts'));
});

Route::get('/announcements/{dept?}/{id?}', 'PostController@posts');


Auth::routes();

Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth', 'throttle:60,1')->group(function () {
    Route::get('/posts', 'PostController@index');
    Route::post('/posts/delete', 'PostController@delete');
    Route::post('/posts/soft-delete', 'PostController@softDelete');
    Route::get('/details-post/{id?}', 'PostController@show');
    Route::post('/details-post/{id?}', 'PostController@updateOrCreate');
    Route::get('/change-password', 'UserController@changePassword');
    Route::post('/change-password', 'UserController@changePass');
    Route::middleware(['admin'])->group(function () {
	    Route::get('/users', 'UserController@index');
        Route::post('/users/change-status', 'UserController@changeStatus');
    	Route::get('/user/{id?}', 'UserController@show');
        Route::post('/user/{id?}', 'UserController@updateOrCreate');
	});
});
